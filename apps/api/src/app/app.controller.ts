import { Controller, Get } from '@nestjs/common';
import { AppService } from './app.service';
import { MessageDto } from '@demo-projects/api-interfaces';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get('hello')
  getData(): MessageDto {
    return this.appService.getData();
  }
}
