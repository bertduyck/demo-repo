import { Injectable } from '@nestjs/common';
import { MessageDto } from '@demo-projects/api-interfaces';

@Injectable()
export class AppService {
  getData(): MessageDto {
    return { message: 'Welcome to api!' };
  }
}
