import {
  ConnectedSocket,
  MessageBody,
  OnGatewayConnection,
  OnGatewayDisconnect, OnGatewayInit,
  SubscribeMessage,
  WebSocketGateway,
  WebSocketServer
} from '@nestjs/websockets';
import { Inject, Logger } from '@nestjs/common';
import { MessageDto } from '@demo-projects/api-interfaces';
import { Socket, Server } from 'socket.io';

@WebSocketGateway()
export class ChatGateway implements OnGatewayConnection, OnGatewayDisconnect, OnGatewayInit {
  private logger: Logger = new Logger('ChatGateway');
  // private messages;

  constructor(@Inject('MESSAGES') private MESSAGES: MessageDto[]) {
    // this.messages = MESSAGES;
  }

  @WebSocketServer() server: Server;

  async handleConnection(@ConnectedSocket() client: Socket) {
    this.logger.log('connected client');
    this.MESSAGES.forEach(msg => client.emit('chat', msg))
  }

  async handleDisconnect() {
    this.logger.log('disconnected client');
  }

  afterInit(server: Server): any {
    this.logger.log('websocket gateway initialized');
  }

  @SubscribeMessage('chat')
  onChat(
    @MessageBody() message: MessageDto,
    @ConnectedSocket() client: Socket
  ) {
    this.logger.log({ message, client: client.id });
    this.MESSAGES.push(message);
    this.logger.log(this.MESSAGES);
    client.broadcast.emit('chat', message);
  }
}
