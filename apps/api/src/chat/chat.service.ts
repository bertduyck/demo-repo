import { Inject, Injectable } from '@nestjs/common';
import { MessageDto } from '@demo-projects/api-interfaces';

@Injectable()
export class ChatService {
  constructor(@Inject('MESSAGES') private MESSAGES: MessageDto[]) {
  }

  getMessages(): MessageDto[] {
    return this.MESSAGES;
  }

  addMessage(message: MessageDto) {
    this.MESSAGES.push(message);
  }

  clearMessages() {
    this.MESSAGES.splice(0, this.MESSAGES.length);
    return this.MESSAGES;
  }
}
