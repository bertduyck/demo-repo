import { Controller, Delete, Get } from '@nestjs/common';
import { ChatService } from './chat.service';
import { MessageDto } from '@demo-projects/api-interfaces';

@Controller('messages')
export class ChatController {

  constructor(private readonly appService: ChatService) {}

  @Get()
  getMessages(): MessageDto[] {
    return this.appService.getMessages();
  }

  @Delete()
  removeMessageHistory() {
    return this.appService.clearMessages();
  }
}
