import { Module } from '@nestjs/common';
import { ChatGateway } from './chat.gateway';
import { ChatService } from './chat.service';
import { ChatController } from './chat.controller';
import { MESSAGES } from './messages';

const tokens = [
  { provide: 'MESSAGES', useValue: MESSAGES}
];

@Module({
  imports: [],
  controllers: [ChatController],
  providers: [ ...tokens, ChatGateway, ChatService]
})
export class ChatModule {}
