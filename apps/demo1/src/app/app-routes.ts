import { Routes } from '@angular/router';

export const appRoutes: Routes = [
  {
    path: "chat",
    loadChildren: () => import("@demo-projects/chat").then(m => m.ChatModule)
  }
];
