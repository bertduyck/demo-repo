module.exports = {
  name: 'demo1',
  preset: '../../jest.config.js',
  coverageDirectory: '../../coverage/apps/demo1',
  snapshotSerializers: [
    'jest-preset-angular/AngularSnapshotSerializer.js',
    'jest-preset-angular/HTMLCommentSerializer.js'
  ]
};
