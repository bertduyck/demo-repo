import { Injectable } from '@angular/core';
import { Socket } from 'ngx-socket-io';
import { Observable } from 'rxjs';
import { MessageDto } from '../dto';
import { tap } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class MessageApiService {
  constructor(private webSocket: Socket, private http: HttpClient) {}

  sendChat(message: MessageDto) {
    this.webSocket.emit('chat', message);
  }

  retrieveChat(): Observable<MessageDto> {
    return this.webSocket.fromEvent('chat').pipe(tap(console.log));
  }

  removeAll() {
    return this.http.delete('http://localhost:3333/api/messages');
  }
}
