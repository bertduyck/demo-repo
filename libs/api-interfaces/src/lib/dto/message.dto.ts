export interface MessageDto {
  message: string;
  userName?: string;
}
