import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { routes } from './chat-routes';
import { ChatOverviewModule } from './chat-overview/chat-overview.module';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [CommonModule, ChatOverviewModule, RouterModule.forChild(routes)],
  declarations: []
})
export class ChatModule {}
