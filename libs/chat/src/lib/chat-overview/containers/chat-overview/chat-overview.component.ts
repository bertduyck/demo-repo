import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { MessageDto } from '@demo-projects/api-interfaces';
import { ChatOverviewSandboxService } from '../../chat-overview-sandbox.service';

@Component({
  selector: 'demo-projects-chat-overview',
  templateUrl: './chat-overview.component.html',
  styleUrls: ['./chat-overview.component.scss']
})
export class ChatOverviewComponent implements OnInit {
  messages$: Observable<MessageDto[]>;

  constructor(private sandbox: ChatOverviewSandboxService) {
  }

  ngOnInit(): void {
    this.messages$ = this.sandbox.getAllMessages();
  }

  sendMessage(message: string) {
    this.sandbox.sendMessage({message});
  }

  clear() {
    this.sandbox.clearMessages().subscribe();
  }
}
