import { Component, EventEmitter, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'demo-projects-chat-message-box',
  templateUrl: './chat-message-box.component.html',
  styleUrls: ['./chat-message-box.component.scss']
})
export class ChatMessageBoxComponent {
  messageForm: FormGroup;

  @Output() chatMessage = new EventEmitter();

  constructor(private formBuilder: FormBuilder) {
    this.messageForm = this.formBuilder.group({
      message: new FormControl()
    })
  }

  onSubmit(form: FormGroup) {
    this.chatMessage.emit(form.value.message);
    this.messageForm.reset();
  }
}
