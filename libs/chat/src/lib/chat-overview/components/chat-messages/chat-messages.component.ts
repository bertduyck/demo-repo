import { Component, Input, OnInit } from '@angular/core';
import { MessageDto } from '@demo-projects/api-interfaces';

@Component({
  selector: 'demo-projects-chat-messages',
  templateUrl: './chat-messages.component.html',
  styleUrls: ['./chat-messages.component.scss']
})
export class ChatMessagesComponent implements OnInit {
  @Input() messages: MessageDto[];

  constructor() { }

  ngOnInit() {
  }
}
