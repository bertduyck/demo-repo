import { TestBed } from '@angular/core/testing';

import { ChatOverviewSandboxService } from './chat-overview-sandbox.service';

describe('ChatOverviewSandboxService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ChatOverviewSandboxService = TestBed.get(ChatOverviewSandboxService);
    expect(service).toBeTruthy();
  });
});
