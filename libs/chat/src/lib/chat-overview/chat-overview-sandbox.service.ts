import { Injectable } from '@angular/core';
import { MessageApiService, MessageDto } from '@demo-projects/api-interfaces';
import { merge, Observable, Subject } from 'rxjs';
import { scan } from 'rxjs/operators';

@Injectable()
export class ChatOverviewSandboxService {
  private readonly messageSendSubject$ = new Subject<MessageDto>();
  private readonly messageSend$ = this.messageSendSubject$.asObservable();
  private readonly messageReceived$ = this.getMessage();

  constructor(private messageApiService: MessageApiService) { }

  getMessage() {
    return this.messageApiService.retrieveChat();
  }

  getAllMessages(): Observable<MessageDto[]> {
    return merge(
      this.messageReceived$,
      this.messageSend$
    ).pipe(
      scan((acc: MessageDto[], curr: MessageDto) => {
        return [...acc, curr];
      }, [])
    );
  }

  sendMessage(message: MessageDto) {
    this.messageApiService.sendChat(message);
    this.messageSendSubject$.next(message);
  }

  clearMessages() {
    return this.messageApiService.removeAll();
  }
}
