import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChatOverviewComponent } from './containers/chat-overview/chat-overview.component';
import { ChatMessageBoxComponent } from './components/chat-message-box/chat-message-box.component';
import { ChatOverviewSandboxService } from './chat-overview-sandbox.service';
import { ReactiveFormsModule } from '@angular/forms';
import { ChatMessagesComponent } from './components/chat-messages/chat-messages.component';

@NgModule({
  declarations: [ChatOverviewComponent, ChatMessageBoxComponent, ChatMessagesComponent],
  imports: [
    CommonModule, ReactiveFormsModule
  ],
  providers: [ChatOverviewSandboxService]
})
export class ChatOverviewModule { }
