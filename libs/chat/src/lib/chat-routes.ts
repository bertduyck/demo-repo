import { Routes } from '@angular/router';
import { ChatOverviewComponent } from './chat-overview/containers/chat-overview/chat-overview.component';

export const routes: Routes = [
  {
    path: "",
    pathMatch: "full",
    component: ChatOverviewComponent
  }
];
